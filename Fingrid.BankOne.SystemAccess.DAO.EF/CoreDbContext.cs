﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.DAO.EF
{
    public partial class CoreDbContext : DbContext
    {
        public CoreDbContext() : base("name=CoreDbContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<CoreDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
