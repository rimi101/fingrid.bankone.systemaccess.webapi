﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.DAO.EF
{
    public partial class LocalDbContext : DbContext
    {
        public LocalDbContext()
            : base("name=LocalDbContext")
        {
        }

        public LocalDbContext(DbConnection dbConnection, bool contextOwnsConnection) : base(dbConnection, contextOwnsConnection)
        {
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<LocalDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
