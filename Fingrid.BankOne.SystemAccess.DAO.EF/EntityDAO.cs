﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using Fingrid.BankOne.SystemAccess.Core.Models;
using Fingrid.BankOne.SystemAccess.DAO.Interfaces;
using Fingrid.BankOne.SystemAccess.Utility;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.DAO.EF
{
    public class EntityDAO<T> : IEntityDAO<T> where T : Entity
    {
        private DbConnection CreateConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }

        private string CreateConnectionString(string connectionString)
        {
            List<string> connStringParameters = connectionString.Split(';').ToList();
            string server = connStringParameters[0].Split('=')[1];
            string initialcatalog = connStringParameters[1].Split('=')[1];
            string userID = connStringParameters[2].Split('=')[1];
            string password = connStringParameters[3].Split('=')[1];

            var builder = new SqlConnectionStringBuilder
            {
                DataSource = server,
                InitialCatalog = initialcatalog,
                IntegratedSecurity = false,
                MultipleActiveResultSets = false,
                PersistSecurityInfo = true,
                UserID = userID,
                Password = password
            };
            return builder.ConnectionString;
        }

        public IEnumerable<T> BulkInsertAndSave(List<T> _objects, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            try
            {
                switch (databaseSource)
                {
                    case DatabaseSource.Local:
                        return null;
                    case DatabaseSource.Remote:
                        return null;
                    case DatabaseSource.Core:
                        return null;
                    case DatabaseSource.SystemAccess:
                        using (SystemAccessDbContext systemAccessDbContext = new SystemAccessDbContext())
                        {
                            var response = systemAccessDbContext.Set<T>().AddRange(_objects);
                            systemAccessDbContext.SaveChanges();
                            return response;
                        }
                    default:
                        return null;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                throw;
            }
        }

        public void Delete(T _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            try
            {
                switch (databaseSource)
                {
                    case DatabaseSource.Local:
                        break;
                    case DatabaseSource.Remote:
                        break;
                    case DatabaseSource.Core:
                        break;
                    case DatabaseSource.SystemAccess:
                        using (SystemAccessDbContext systemAccessDbContext = new SystemAccessDbContext())
                        {
                            systemAccessDbContext.Set<T>().Attach(_object);
                            systemAccessDbContext.Set<T>().Remove(_object);
                            systemAccessDbContext.SaveChanges();
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                throw;
            }
        }

        public void ExecuteQuery(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            throw new NotImplementedException();
        }

        public List<T> ExecuteQuery_List(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            throw new NotImplementedException();
        }

        public T ExecuteQuery_Object(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> GetAll(string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            try
            {
                switch (databaseSource)
                {
                    case DatabaseSource.Local:
                        return null;
                    case DatabaseSource.Remote:
                        return null;
                    case DatabaseSource.Core:
                        return null;
                    case DatabaseSource.SystemAccess:
                        using (SystemAccessDbContext systemAccessDbContext = new SystemAccessDbContext())
                        {
                            IEnumerable<T> result = systemAccessDbContext.Set<T>().Select(x => x).ToList();
                            return result;
                        }
                    default:
                        return null;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                throw;
            }
        }

        public T GetByID(long ID, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            try
            {
                switch (databaseSource)
                {
                    case DatabaseSource.Local:
                        return null;
                    case DatabaseSource.Remote:
                        return null;
                    case DatabaseSource.Core:
                        return null;
                    case DatabaseSource.SystemAccess:
                        using (SystemAccessDbContext systemAccessDbContext = new SystemAccessDbContext())
                        {
                            T result = systemAccessDbContext.Set<T>().Find(ID);
                            return result;
                        }
                    default:
                        return null;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                throw;
            }
        }

        public PagedList<T> GetByPage(string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess, PaginationOrder paginationOrder = PaginationOrder.DESC, int page = 1, int pageSize = 10)
        {
            try
            {
                switch (databaseSource)
                {
                    case DatabaseSource.Local:
                        return null;
                    case DatabaseSource.Remote:
                        return null;
                    case DatabaseSource.Core:
                        return null;
                    case DatabaseSource.SystemAccess:
                        using (SystemAccessDbContext systemACcessDbContext = new SystemAccessDbContext())
                        {
                            long total = systemACcessDbContext.Set<T>().Count();
                            IEnumerable<T> items;
                            switch (paginationOrder)
                            {
                                case PaginationOrder.DESC:
                                    items = systemACcessDbContext.Set<T>().AsNoTracking().Where(x => x.InstitutionCode == mfbCode).Select(x => x).OrderByDescending(x => x.ID).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                                    break;
                                case PaginationOrder.ASC:
                                    items = systemACcessDbContext.Set<T>().AsNoTracking().Where(x => x.InstitutionCode == mfbCode).Select(x => x).OrderBy(x => x.ID).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                                    break;
                                default:
                                    items = systemACcessDbContext.Set<T>().AsNoTracking().Where(x => x.InstitutionCode == mfbCode).Select(x => x).OrderByDescending(x => x.ID).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                                    break;
                            }
                            PagedList<T> pagedList = new PagedList<T>
                            {
                                data = items,
                                total = total,
                                page = page,
                                pagesize = pageSize
                            };
                            return pagedList;
                        }
                    default:
                        return null;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                throw;
            }
        }

        public T InsertAndSave(T _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            try
            {
                switch (databaseSource)
                {
                    case DatabaseSource.Local:
                        return null;
                    case DatabaseSource.Remote:
                        return null;
                    case DatabaseSource.Core:
                        return null;
                    case DatabaseSource.SystemAccess:
                        using (SystemAccessDbContext systemAccessDbContext = new SystemAccessDbContext())
                        {
                            var data = systemAccessDbContext.Set<T>().Add(_object);
                            systemAccessDbContext.SaveChanges();
                            return data;
                        }
                    default:
                        return null;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                throw;
            }
        }

        public T Update(T _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            try
            {
                switch (databaseSource)
                {
                    case DatabaseSource.Local:
                        return null;
                    case DatabaseSource.Remote:
                        return null;
                    case DatabaseSource.Core:
                        return null;
                    case DatabaseSource.SystemAccess:
                        using (SystemAccessDbContext systemAccessDbContext = new SystemAccessDbContext())
                        {
                            var data = systemAccessDbContext.Set<T>().Attach(_object);
                            systemAccessDbContext.Entry(_object).State = System.Data.Entity.EntityState.Modified;
                            systemAccessDbContext.SaveChanges();
                            return data;
                        }
                    default:
                        return null;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                throw;
            }
        }
    }
}
