﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using Fingrid.BankOne.SystemAccess.Core.Models;
using Fingrid.BankOne.SystemAccess.DAO.Interfaces;
using Fingrid.BankOne.SystemAccess.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Fingrid.BankOne.SystemAccess.DAO.EF.Implementation
{
    public class VerificationCodeDAO : EntityDAO<VerificationCode>, IVerificationCodeDAO
    {
        public VerificationCode GetByCode(string mfbCode, string code, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            try
            {
                using (SystemAccessDbContext systemAccessDbContext = new SystemAccessDbContext())
                {
                    VerificationCode verCode = systemAccessDbContext.VerificationCodes.Where(x => x.InstitutionCode == mfbCode && x.Code == code).FirstOrDefault();
                    return verCode;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                throw;
            }
        }

        public PagedList<VerificationCode> RetrievePaged(string mfbCode, string verificationCode, string generatedBy, string requestedBy, string status, int page, int perPage, DatabaseSource databaseSource)
        {
            try
            {
                using (SystemAccessDbContext systemAccessDbContext = new SystemAccessDbContext())
                {
                    long total = systemAccessDbContext.Set<VerificationCode>().Where(x =>
                    x.InstitutionCode == mfbCode
                    && x.Code.Contains(verificationCode)
                    && x.UserName.Contains(generatedBy)
                    && x.RequestBy.Contains(requestedBy)
                    //&& x.Status.ToString().Contains("")
                    //&& Regex.IsMatch(x.Code, $".*{verificationCode}.*") &&
                    //Regex.IsMatch(x.UserName, $".*{generatedBy}.*") &&
                    //Regex.IsMatch(x.RequestBy, $".*{requestedBy}.*") &&
                    //Regex.IsMatch(x.Status.ToString(), $".*{status}.*")
                    //Enum.IsDefined(typeof(SystemAccessCodeStatus), status) ?
                    //Regex.IsMatch(x.Status.ToString(), $".*{Enum.Parse(typeof(SystemAccessCodeStatus), status)}.*") : false
                    ).Count();
                    var items = systemAccessDbContext.VerificationCodes.Where(x => 
                    x.InstitutionCode == mfbCode
                    && x.Code.Contains(verificationCode)
                    && x.UserName.Contains(generatedBy)
                    && x.RequestBy.Contains(requestedBy)
                    //&& x.Status.ToString().Contains("")
                    //&& Regex.IsMatch(x.Code, $".*{verificationCode}.*") &&
                    //Regex.IsMatch(x.UserName, $".*{generatedBy}.*") &&
                    //Regex.IsMatch(x.RequestBy, $".*{requestedBy}.*") &&
                    //Regex.IsMatch(x.Status.ToString(), $".*{status}.*")
                    ).Select(x => x).OrderByDescending(x => x.ID).Skip((page - 1) * perPage).Take(perPage).ToList();

                    PagedList<VerificationCode> pagedList = new PagedList<VerificationCode>
                    {
                        data = items,
                        total = total,
                        page = page,
                        pagesize = perPage
                    };
                    return pagedList;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                throw;
            }
        }
    }
}
