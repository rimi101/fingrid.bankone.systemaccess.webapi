﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using Fingrid.BankOne.SystemAccess.Core.Models;
using Fingrid.BankOne.SystemAccess.DAO.Interfaces;
using Fingrid.BankOne.SystemAccess.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.DAO.EF.Implementation
{
    public class ValidationCodeDAO : EntityDAO<ValidationCode>, IValidationCodeDAO
    {
        public ValidationCode GetByCode(string mfbCode, string code, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            try
            {
                using (SystemAccessDbContext systemAccessDbContext = new SystemAccessDbContext())
                {
                    ValidationCode valCode = systemAccessDbContext.ValidationCodes.Where(x => x.Code == code && x.InstitutionCode == mfbCode).FirstOrDefault();
                    return valCode;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                throw;
            }
        }
    }
}
