﻿using Fingrid.BankOne.SystemAccess.Core.Models;
using Fingrid.BankOne.SystemAccess.DAO.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.DAO.EF.Implementation
{
    public class InstitutionStatusDAO : EntityDAO<InstitutionStatus>, IInstitutionStatusDAO
    {
        public InstitutionStatus GetByCode(string mfbCode)
        {
            using (SystemAccessDbContext systemAccessDbContext = new SystemAccessDbContext())
            {
                var institutionStatus = systemAccessDbContext.InstitutionStatuses.Where(x => x.InstitutionCode == mfbCode).FirstOrDefault();
                return institutionStatus;
            }
        }
    }
}
