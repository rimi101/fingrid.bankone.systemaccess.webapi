﻿using Fingrid.BankOne.SystemAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.DAO.EF
{
    public partial class SystemAccessDbContext : DbContext
    {
        public SystemAccessDbContext()
            : base("name=SystemAccessDbContext")
        {
        }

        public virtual DbSet<VerificationCode> VerificationCodes { get; set; }

        public virtual DbSet<ValidationCode> ValidationCodes { get; set; }

        public virtual DbSet<InstitutionStatus> InstitutionStatuses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<SystemAccessDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
