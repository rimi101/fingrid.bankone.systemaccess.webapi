﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.DAO.EF
{
    public partial class RemoteDbContext : DbContext
    {
        public RemoteDbContext()
            : base("name=RemoteDbContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<RemoteDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
