﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Utility
{
    public class Logger
    {
        public void LogException(Exception ex)
        {
            LogException(ex, "");
        }

        private void LogException(Exception ex, string namePrefix)
        {
            Log("***Error****", 1, namePrefix);
            try
            {
                Log(string.Format("{0}  \n{1}", GetInnerMostException(ex).Message, ex.StackTrace), 1, namePrefix);
            }
            catch
            {
                Log("Message part of Exception has issues.", 1, namePrefix);
            }

            Log("***End Error****", 2, namePrefix);
        }

        private void Log(string description, int pad, string namePrefix)
        {
            Trace.TraceInformation(string.Format("[{0}] : {1}", DateTime.Now.ToString("dd-MMM-yy hh:mm:ss.fffffffK"), description));
        }

        private void Log(string description, int pad)
        {
            Log(description, pad, "");
        }

        public void Log(string description)
        {
            Log(description, 1, "");
        }

        private Exception GetInnerMostException(Exception ex)
        {
            if (ex.InnerException != null)
                return GetInnerMostException(ex.InnerException);
            return ex;
        }
    }
}
