﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Utility
{
    public class Code
    {
        public static string Generate
        {
            get
            {
                return Math.Abs((DateTime.Now.Millisecond + DateTime.Now.Second + DateTime.Now.Minute + DateTime.Now.Hour + DateTime.Now.Day + DateTime.Now.Month) * DateTime.Now.Year).ToString();
            }
        }
    }
}
