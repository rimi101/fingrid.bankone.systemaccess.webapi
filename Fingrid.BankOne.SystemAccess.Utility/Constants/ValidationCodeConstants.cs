﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Utility.Constants
{
    public class ValidationCodeConstants
    {
        public const string VALIDATIONCODE_NOT_MATCH_INSTITUTIONCODE = "The ValidationCode does not match the InstitutionCode.";

        public const string INVALID_VALIDATION_CODE = "Invalid ValidationCode.";

        public const string VALIDATIONCODE_NOT_MATCH_CREATIONDATE = "Invalid ValidationCode.";

        public const string VALIDATIONCODE_VERIFICATIONCODE_NOT_FOUND = "The VerificationCode tied to this ValidationCode could not be located.";
    }
}
