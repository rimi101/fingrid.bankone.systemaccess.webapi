﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Utility.Constants
{
    public class VerificationCodeConstants
    {
        public const string VERIFICATIONCODE_NOT_FOUND = "VerificationCode Not Found!";

        public const string INVALID_VERIFICATION_CODE = "Invalid Verification Code";

        public const string EXPIRED_VERIFICATION_CODE = "Expired Verification Code";

        public const string VERIFICATIONCODE_VERIFIED_SUCCESSFULLY = "VerificationCode Verified Successfully";
    }
}
