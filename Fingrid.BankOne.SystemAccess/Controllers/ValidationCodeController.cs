﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using Fingrid.BankOne.SystemAccess.Core.Models;
using Fingrid.BankOne.SystemAccess.DAO.EF.Implementation;
using Fingrid.BankOne.SystemAccess.Models.Request;
using Fingrid.BankOne.SystemAccess.Models.Request.ValidationCode;
using Fingrid.BankOne.SystemAccess.Models.Response;
using Fingrid.BankOne.SystemAccess.Services.Implementations;
using Fingrid.BankOne.SystemAccess.Utility;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Fingrid.BankOne.SystemAccess.Controllers
{
    /// <summary>
    /// Controller for Validation Codes
    /// </summary>
    public class ValidationCodeController : ApiController
    {
        private readonly ValidationCodeService validationCodeService;
        private readonly VerificationCodeService verificationCodeService;
        /// <summary>
        /// constructor
        /// </summary>
        public ValidationCodeController()
        {
            validationCodeService = new ValidationCodeService(new ValidationCodeDAO());
            verificationCodeService = new VerificationCodeService(new VerificationCodeDAO());
        }

        /// <summary>
        /// Returns a list of all the ValidationCodes available
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ValidationCode))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, validationCodeService.GetAll(""));
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Returns a paginated instance of the list of validation codes
        /// </summary>
        /// <param name="item">pagination request parameters</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(PagedList<ValidationCode>))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        public HttpResponseMessage GetPaged([FromUri] Paged item)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, validationCodeService.GetByPage(item.mfbCode, Core.Enums.DatabaseSource.SystemAccess));//, (PaginationOrder)item.PaginationOrder, item.Page, item.PageSize));
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Enable a validation code
        /// </summary>
        /// <param name="mfbCode">the institution's 4-digit code</param>
        /// <param name="Code">the validation code </param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ValidationCode))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        public HttpResponseMessage Enable(string mfbCode, string Code)
        {
            try
            {
                var valCode = validationCodeService.GetByCode(mfbCode, Code);
                var verCode = verificationCodeService.GetByCode(mfbCode, valCode.VerificationCode);
                verCode.Status = SystemAccessCodeStatus.Enabled;
                return Request.CreateResponse(HttpStatusCode.OK, verificationCodeService.Update(verCode, mfbCode));
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Create a new validation code
        /// </summary>
        /// <param name="item">parameters of the new validation code to be created</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ValidationCode))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        public HttpResponseMessage Save([FromUri] Save item)
        {
            try
            {
                ValidationCode validationCode = new ValidationCode
                {
                    InstitutionCode = item.InstitutionCode,
                    Code = item.Code,
                    VerificationCode = item.verificationCode,
                    CreationDate = item.CreationDate
                };
                return Request.CreateResponse(HttpStatusCode.OK, validationCodeService.InsertAndSave(validationCode, item.InstitutionCode));
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Vadlidate a [validation combo] to ensure that its combination is correct
        /// </summary>
        /// <param name="institutionCode">the institution's 4-digit code</param>
        /// <param name="validationCode">the validation code combo</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(string))]
        [SwaggerResponse(HttpStatusCode.NotFound, Type = typeof(ValidationResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        public HttpResponseMessage Validate(string validationCode, string institutionCode)
        {
            try
            {
                List<string> valSections = validationCode.Split(new[] { "||" }, StringSplitOptions.None).ToList();
                foreach (var item in valSections)
                {
                    string[] section = item.Split('-');
                    if (institutionCode == section[0])
                    {
                        string _response;
                        bool validationStatus = ValidateValidationCode(section[0], section[1], section[2], out _response);
                        ValidationResponse response = new ValidationResponse
                        {
                            IsValidated = validationStatus,
                            ResponseMessage = _response,
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, response);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Vadlidate a [validation combo] to ensure that its combination is correct
        /// </summary>
        /// <param name="institutionCode">the institution's 4-digit code</param>
        /// <param name="validationCode">the validation code combo</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(string))]
        [SwaggerResponse(HttpStatusCode.NotFound, Type = typeof(ValidationResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        public HttpResponseMessage Validate2(string validationCode, string institutionCode)
        {
            try
            {
                List<string> valSections = validationCode.Split(new[] { "||" }, StringSplitOptions.None).ToList();
                foreach (var item in valSections)
                {
                    string[] section = item.Split('-');
                    if (institutionCode == section[0])
                    {
                        string _response;
                        bool validationStatus = ValidateValidationCode2(section[0], section[1], section[2], out _response);
                        ValidationResponse response = new ValidationResponse
                        {
                            IsValidated = validationStatus,
                            ResponseMessage = _response,
                        };
                        if (validationStatus)
                            return Request.CreateResponse(HttpStatusCode.OK, response);
                        else
                            return Request.CreateResponse(HttpStatusCode.NotFound, response);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }


        private bool ValidateValidationCode(string institutionCode, string code, string date, out string responseMessage)
        {
            DateTime _newDate = Convert.ToDateTime(date, CultureInfo.InvariantCulture);
            ValidationCode checkValCode = validationCodeService.GetByCode(institutionCode, code);
            if (checkValCode == null)// || ))
            {
                responseMessage = "The ValidationCode could not be recognised.";
                return false;
            }
            if (checkValCode.CreationDate.Value != Convert.ToDateTime(date, CultureInfo.InvariantCulture))
            {
                responseMessage = "The ValidationCode CreationDate does not match.";
                return false;
            }
            else
            {
                //responseMessage = $"Access Granted for user with VerificationCode: {code}";
                //return true;
                Core.Models.VerificationCode checkVerCode = verificationCodeService.GetByCode(institutionCode, checkValCode.VerificationCode);
                if (checkVerCode == null)
                {
                    responseMessage = "The VerificationCode tied to this ValidationCode could not be located.";
                    return false;
                }
                else
                {
                    if (checkVerCode.Status == SystemAccessCodeStatus.Enabled)
                    {
                        responseMessage = $"Access Granted for user with VerificationCode: {checkVerCode.Code}";
                        return true;
                    }
                    else
                    {
                        responseMessage = $"Access Denied for user with VerificationCode: {checkVerCode.Code}. \nStatus is {Enum.GetName(typeof(SystemAccessCodeStatus), checkVerCode.Status)}. \nId is {checkVerCode.ID}";
                        return false;
                    }
                }
            }
        }

        private bool ValidateValidationCode2(string institutionCode, string code, string date, out string responseMessage)
        {
            DateTime _newDate = Convert.ToDateTime(date, CultureInfo.InvariantCulture);
            ValidationCode checkValCode = validationCodeService.GetByCode(institutionCode, code);
            if (checkValCode == null)// || ))
            {
                responseMessage = "The ValidationCode could not be recognised.";
                return false;
            }
            if (checkValCode.CreationDate.Value != Convert.ToDateTime(date, CultureInfo.InvariantCulture))
            {
                responseMessage = $"The ValidationCode CreationDate does not match. ({checkValCode.CreationDate.Value} != {Convert.ToDateTime(date, CultureInfo.InvariantCulture)})";
                return false;
            }
            else
            {
                Core.Models.VerificationCode checkVerCode = verificationCodeService.GetByCode(institutionCode, checkValCode.VerificationCode);
                if (checkVerCode == null)
                {
                    responseMessage = "The VerificationCode tied to this ValidationCode could not be located.";
                    return false;
                }
                else
                {
                    if (checkVerCode.Status == SystemAccessCodeStatus.Enabled)
                    {
                        responseMessage = $"Access Granted for user with VerificationCode: {checkVerCode.Code}";
                        return true;
                    }
                    else
                    {
                        responseMessage = $"Access Denied for user with VerificationCode: {checkVerCode.Code}. \nStatus is {Enum.GetName(typeof(SystemAccessCodeStatus), checkVerCode.Status)}. \nId = {checkVerCode.ID}";
                        return false;
                    }
                }
            }
        }

    }
}
