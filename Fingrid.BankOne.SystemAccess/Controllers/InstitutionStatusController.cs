﻿using Fingrid.BankOne.SystemAccess.Core.Models;
using Fingrid.BankOne.SystemAccess.DAO.EF.Implementation;
using Fingrid.BankOne.SystemAccess.Models.Request.InstitutionStatus;
using Fingrid.BankOne.SystemAccess.Services.Implementations;
using Fingrid.BankOne.SystemAccess.Utility;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Fingrid.BankOne.SystemAccess.Controllers
{
    /// <summary>
    /// Controller for InstitutionStatus
    /// </summary>
    public class InstitutionStatusController : ApiController
    {
        private readonly InstitutionStatusService institutionStatusService;
        /// <summary>
        /// constructor
        /// </summary>
        public InstitutionStatusController()
        {
            institutionStatusService = new InstitutionStatusService(new InstitutionStatusDAO());
        }

        /// <summary>
        /// Returns a list of all the InstitutionStatuses available
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<InstitutionStatus>))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type=typeof(Exception))]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, institutionStatusService.GetAll(""));
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Returns an institutionStatus object by the mfbCode
        /// </summary>
        /// <param name="mfbCode">the institution's 4-digit code</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(bool))]
        [SwaggerResponse(HttpStatusCode.NotFound, Type = typeof(string))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        public HttpResponseMessage GetByMfbCode(string mfbCode)
        {
            try
            {
                var institutionStatus = institutionStatusService.GetByCode(mfbCode);
                if (institutionStatus != null)
                    return Request.CreateResponse(HttpStatusCode.OK, institutionStatus.Enabled);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, $"No institution found with mfbCode {mfbCode}");
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Profile a new institution for System Access [plugin based]
        /// </summary>
        /// <param name="item">the setup parameters</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(InstitutionStatus))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(string))]
        public HttpResponseMessage SetUpNewInstitution([FromUri] SetUp item)
        {
            try
            {
                InstitutionStatus institutionStatus = institutionStatusService.GetByCode(item.InstitutionCode);
                if (institutionStatus != null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Institution is already profiled for System Access.");
                else
                {
                    InstitutionStatus newEntry = new InstitutionStatus
                    {
                        InstitutionCode = item.InstitutionCode,
                        InstitutionName = item.InstitutionName,
                        Enabled = false
                    };
                    institutionStatus = institutionStatusService.InsertAndSave(newEntry, item.InstitutionCode);
                    return Request.CreateResponse(HttpStatusCode.OK, institutionStatus);
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Enable or Disable an institution that has already been profiled for System Access [plugin based]
        /// </summary>
        /// <param name="institutionCode"> the institution's 4-digit code</param>
        /// <param name="enabled">the status </param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(InstitutionStatus))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(string))]
        public HttpResponseMessage EnableDisableInstitution(string institutionCode, bool enabled)
        {
            try
            {
                InstitutionStatus institutionStatus = institutionStatusService.GetByCode(institutionCode);
                if (institutionStatus == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, $"Institution not found! Code: {institutionCode}");
                else
                {
                    institutionStatus.Enabled = enabled;
                    return Request.CreateResponse(HttpStatusCode.OK, institutionStatusService.Update(institutionStatus, institutionCode));
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Get an institutionStatus by id
        /// </summary>
        /// <param name="id">the id</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(InstitutionStatus))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        [SwaggerResponse(HttpStatusCode.NotFound, Type = typeof(string))]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                InstitutionStatus institutionStatus = institutionStatusService.GetByID(id, "");
                if (institutionStatus == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, $"No institution status found with ID: [{id}]");
                else
                    return Request.CreateResponse(HttpStatusCode.OK, institutionStatus);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

    }
}
