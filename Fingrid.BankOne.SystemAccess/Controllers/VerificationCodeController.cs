﻿using Fingrid.BankOne.SystemAccess.AuditTrailServiceRef;
using Fingrid.BankOne.SystemAccess.Core.Enums;
using Fingrid.BankOne.SystemAccess.Core.Models;
using Fingrid.BankOne.SystemAccess.DAO.EF.Implementation;
using Fingrid.BankOne.SystemAccess.Models.Request;
using Fingrid.BankOne.SystemAccess.Models.Request.VerificationCode;
using Fingrid.BankOne.SystemAccess.Services.Implementations;
using Fingrid.BankOne.SystemAccess.Utility;
using Fingrid.BankOne.SystemAccess.Utility.Constants;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VerificationCode = Fingrid.BankOne.SystemAccess.Core.Models.VerificationCode;

namespace Fingrid.BankOne.SystemAccess.Controllers
{
    /// <summary>
    /// Controller for VerificationCodes
    /// </summary>
    public class VerificationCodeController : ApiController
    {
        private readonly VerificationCodeService verificationCodeService;
        /// <summary>
        /// constructor
        /// </summary>
        public VerificationCodeController()
        {
            verificationCodeService = new VerificationCodeService(new VerificationCodeDAO());
        }

        /// <summary>
        /// Returns a list of all the VerificationCodes available
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(VerificationCode))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, verificationCodeService.GetAll(""));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Returns a paginated instance of the list of verification codes
        /// </summary>
        /// <param name="item">pagination request parameters</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(PagedList<VerificationCode>))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        public HttpResponseMessage GetPaged([FromUri] Paged item)
        {
            try
            {
                var response = verificationCodeService.GetByPage(item.mfbCode, Core.Enums.DatabaseSource.SystemAccess).data;
                return Request.CreateResponse(HttpStatusCode.OK, response);//, (PaginationOrder)item.PaginationOrder, item.Page, item.PageSize));
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        public HttpResponseMessage RetrievePaged(string mfbCode, string verificationCode = "", string generatedBy = "", string requestedBy = "", string status = "", int page = 1, int perPage = 10)
        {
            try
            {
                var response = verificationCodeService.RetrievePaged(mfbCode, verificationCode, generatedBy, requestedBy, status, page, perPage);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Generates a new system access verification code
        /// </summary>
        /// <param name="item">parameters passed to be used to generate a verification code</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(VerificationCode))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        public HttpResponseMessage Generate([FromUri] Generate item)
        {
            try
            {
                new Logger().Log($"about to generate verification code: {JsonConvert.SerializeObject(item)}");
                if (string.IsNullOrEmpty(item.MfbCode))
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "");
                string code = Code.Generate;
                VerificationCode verificationCode = new VerificationCode
                {
                    InstitutionCode = item.MfbCode,
                    Status = Core.Enums.SystemAccessCodeStatus.Generated,
                    UserName = item.UserName,
                    DateGenerated = DateTime.Now,
                    DateVerified = null,
                    RequestResponse = null,
                    RequestBy = item.RequestBy,
                    Code = code,
                    RequestPurpose = item.RequestPurpose
                };
                var verCode = verificationCodeService.InsertAndSave(verificationCode, "");

                //log audit trail
                using (AuditTrailServiceClient client = new AuditTrailServiceClient())
                {
                    var auditTrailItem = new AuditTrailObj()
                    {
                        Action = AuditActionObj.CREATE,
                        InstitutionCode = item.MfbCode,
                        ActionDate = DateTime.Now,
                        ClientName = item.ClientName,
                        ClientIPAddress = item.ClientIPAddress,
                        UserName = item.UserName,
                        DataType = "System Access Verification Code",
                        SubjectIdentifier = "System Access Verification Code Generation"
                    };
                    var dataAfter = new AuditTrailServiceRef.VerificationCode
                    {
                        InstitutionCode = verCode.InstitutionCode,
                        Status = Enum.GetName(typeof(Core.Enums.SystemAccessCodeStatus), verCode.Status),
                        UserName = verCode.UserName,
                        DateGenerated = verCode.DateGenerated,
                        DateVerified = verCode.DateVerified,
                        Code = verCode.Code,
                        RequestPurpose = verCode.RequestResponse,
                        RequestBy = verCode.RequestBy,
                        ID = Convert.ToInt32(verCode.ID)
                    };
                    //var result = client.BuildAndSaveAuditTrailItem2(item.MfbCode, auditTrailItem, null, dataAfter);
                };
                return Request.CreateResponse(HttpStatusCode.OK, verCode.Code);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// update the status of a verification code
        /// </summary>
        /// <param name="item">parameters required for updating a verification code</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.NotFound, Type = typeof(string))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(VerificationCode))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        public HttpResponseMessage UpdateStatus([FromUri] UpdateStatus item)
        {
            try
            {
                var verCode = verificationCodeService.GetByID(Convert.ToInt64(item.ID), item.mfbCode);

                if (verCode == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, $"No verification code found with ID: [{item.ID}]");
                else
                {
                    using (AuditTrailServiceClient client = new AuditTrailServiceClient())
                    {
                        var auditTrailItem = new AuditTrailObj()
                        {
                            Action = AuditActionObj.UPDATE,
                            InstitutionCode = item.mfbCode,
                            ActionDate = DateTime.Now,
                            ClientName = item.ClientName,
                            ClientIPAddress = item.ClientIPAddress,
                            ApplicationName = "Home",
                            UserName = item.UserName,
                            DataType = "System Access Verification Code",
                            SubjectIdentifier = "System Access Verification Code Update"
                        };

                        var dataBefore = new AuditTrailServiceRef.VerificationCode
                        {
                            InstitutionCode = verCode.InstitutionCode,
                            Status = Enum.GetName(typeof(SystemAccessCodeStatus), verCode.Status),
                            UserName = verCode.UserName,
                            DateGenerated = verCode.DateGenerated,
                            DateVerified = verCode.DateVerified,
                            Code = verCode.Code,
                            RequestBy = verCode.RequestBy,
                            RequestPurpose = verCode.RequestPurpose,
                            ID = Convert.ToInt32(verCode.ID)
                        };

                        var dataAfter = new AuditTrailServiceRef.VerificationCode
                        {
                            InstitutionCode = verCode.InstitutionCode,
                            Status = Enum.GetName(typeof(SystemAccessCodeStatus), item.Status),
                            UserName = verCode.UserName,
                            DateGenerated = verCode.DateGenerated,
                            DateVerified = verCode.DateVerified,
                            Code = verCode.Code,
                            RequestBy = verCode.RequestBy,
                            RequestPurpose = verCode.RequestPurpose,
                            ID = Convert.ToInt32(verCode.ID)
                        };

                        var result = client.BuildAndSaveAuditTrailItem2(item.mfbCode, auditTrailItem, dataBefore, dataAfter);
                        verCode.Status = (SystemAccessCodeStatus)Enum.ToObject(typeof(SystemAccessCodeStatus), item.Status);

                        return Request.CreateResponse(HttpStatusCode.OK, verificationCodeService.Update(verCode, item.mfbCode));
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Verify a verification code
        /// </summary>
        /// <param name="item">parameters passed to verify a verification code</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(string))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(VerificationCode))]
        [SwaggerResponse(HttpStatusCode.NotFound, Type = typeof(string))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(Exception))]
        public HttpResponseMessage Verify([FromUri] Verify item)
        {
            try
            {
                string response = string.Empty;
                var verificationCode = verificationCodeService.VerifyVerificationCode(item.MfbCode, item.Code, out response);
                new Logger().Log(item.MfbCode);
                new Logger().Log(item.Code);
                new Logger().Log(response);
                switch (response)
                {
                    case VerificationCodeConstants.EXPIRED_VERIFICATION_CODE:
                    case VerificationCodeConstants.INVALID_VERIFICATION_CODE:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                    case VerificationCodeConstants.VERIFICATIONCODE_NOT_FOUND:
                        return Request.CreateResponse(HttpStatusCode.NotFound, response);
                    case VerificationCodeConstants.VERIFICATIONCODE_VERIFIED_SUCCESSFULLY:
                        return Request.CreateResponse(HttpStatusCode.OK, response);
                    default:
                        return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }


                /*
                Core.Models.VerificationCode verCode = verificationCodeService.GetByCode(item.MfbCode, item.Code);
                if (verCode != null)
                {
                    if (verCode.Status == Core.Enums.SystemAccessCodeStatus.Generated)
                    {
                        verCode.Status = (((TimeSpan)(DateTime.Now - verCode.DateGenerated)).Days > 1 ? Core.Enums.SystemAccessCodeStatus.Expired : Core.Enums.SystemAccessCodeStatus.Verified);
                        verCode.DateVerified = DateTime.Now;
                        var response = verificationCodeService.Update(verCode, item.MfbCode);
                        return Request.CreateResponse(HttpStatusCode.OK, response);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.BadRequest, VerificationCodeConstants.INVALID_VERIFICATION_CODE);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, VerificationCodeConstants.VERIFICATIONCODE_NOT_FOUND);
                */
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex.InnerException);
                new Logger().LogException(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
