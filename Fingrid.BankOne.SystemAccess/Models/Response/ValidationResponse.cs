﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fingrid.BankOne.SystemAccess.Models.Response
{
    public class ValidationResponse
    {
        public bool IsValidated { get; set; }
        public string ResponseMessage { get; set; }
    }
}