﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fingrid.BankOne.SystemAccess.Models.Request.InstitutionStatus
{
    /// <summary>
    /// parameters to profile a new institution for system access
    /// </summary>
    public class SetUp
    {
        /// <summary>
        /// the institution's 4-digit code
        /// </summary>
        [Required]
        public string InstitutionCode { get; set; }

        /// <summary>
        /// the name of the institution
        /// </summary>
        [Required]
        public string InstitutionName { get; set; }
    }
}