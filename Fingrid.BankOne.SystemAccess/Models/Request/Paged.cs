﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fingrid.BankOne.SystemAccess.Models.Request
{
    public class Paged
    {
        internal readonly PaginationOrder PaginationOrder = PaginationOrder.DESC;
        public string mfbCode { get; set; }
        internal readonly int Page = 1;
        internal readonly int PageSize = 15;

        //public string MfbCode { get; internal set; }
        //public object PaginationOrder { get; set; } = PaginationOrder.
    }
}