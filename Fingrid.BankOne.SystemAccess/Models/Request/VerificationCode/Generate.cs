﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fingrid.BankOne.SystemAccess.Models.Request.VerificationCode
{
    /// <summary>
    /// parameters used to create a new verification code
    /// </summary>
    public class Generate
    {
        /// <summary>
        /// the institution's 4-digit code
        /// </summary>
        [Required]
        public string MfbCode { get; set; }
        /// <summary>
        /// the login username 
        /// </summary>
        [Required]
        public string UserName { get; set; }
        /// <summary>
        /// requestBy
        /// </summary>
        [Required]
        public string RequestBy { get; set; }
        /// <summary>
        /// requestPurpose
        /// </summary>
        [Required]
        public string RequestPurpose { get; set; }
        /// <summary>
        /// the name of the client
        /// </summary>
        [Required]
        public string ClientName { get; set; }
        /// <summary>
        /// the IP Address of the client
        /// </summary>
        [Required]
        public string ClientIPAddress { get; set; }
    }
}