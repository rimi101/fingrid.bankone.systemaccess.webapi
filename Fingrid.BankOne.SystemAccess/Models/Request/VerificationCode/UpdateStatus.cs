﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fingrid.BankOne.SystemAccess.Models.Request.VerificationCode
{
    /// <summary>
    /// parameters required for updating a verification code
    /// </summary>
    public class UpdateStatus
    {
        /// <summary>
        /// the institution's 4-digit code
        /// </summary>
        public string mfbCode { get; set; }
        /// <summary>
        /// the institution ID
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// the status
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// the username
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// the clientName
        /// </summary>
        public string ClientName { get; set; }
        /// <summary>
        /// the clientIPAddress
        /// </summary>
        public string ClientIPAddress { get; set; }
    }
}