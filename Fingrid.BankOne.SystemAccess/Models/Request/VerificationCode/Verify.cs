﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fingrid.BankOne.SystemAccess.Models.Request.VerificationCode
{
    /// <summary>
    /// parameters passed to verify a verification code
    /// </summary>
    public class Verify
    {
        /// <summary>
        /// the institution's 4-digit code
        /// </summary>
        [Required]
        public string MfbCode { get; set; }

        /// <summary>
        /// the verification code
        /// </summary>
        [Required]
        public string Code { get; set; }
    }
}