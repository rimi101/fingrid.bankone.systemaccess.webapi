﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fingrid.BankOne.SystemAccess.Models.Request.ValidationCode
{
    /// <summary>
    /// parameters passed to create a new validation code
    /// </summary>
    public class Save
    {
        /// <summary>
        /// the validation code 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// the verification code linked to the validation code
        /// </summary>
        public string verificationCode { get; set; }
        /// <summary>
        /// the date it was created
        /// </summary>
        public DateTime CreationDate { get; set; }
        /// <summary>
        /// the institution's 4-digit code
        /// </summary>
        public string InstitutionCode { get; set; }
    }
}