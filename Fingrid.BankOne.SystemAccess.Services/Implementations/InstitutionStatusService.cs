﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using Fingrid.BankOne.SystemAccess.Core.Models;
using Fingrid.BankOne.SystemAccess.DAO.Interfaces;
using Fingrid.BankOne.SystemAccess.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Services.Implementations
{
    public class InstitutionStatusService : IInstitutionStatusService
    {
        private readonly IInstitutionStatusDAO institutionStatusDAO;
        public InstitutionStatusService(IInstitutionStatusDAO institutionStatusDAO)
        {
            this.institutionStatusDAO = institutionStatusDAO;
        }
        public IEnumerable<InstitutionStatus> BulkInsertAndSave(List<InstitutionStatus> _objects, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return institutionStatusDAO.BulkInsertAndSave(_objects, mfbCode);
        }

        public void Delete(InstitutionStatus _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            institutionStatusDAO.Delete(_object, mfbCode);
        }

        public void ExecuteQuery(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            throw new NotImplementedException();
        }

        public List<InstitutionStatus> ExecuteQuery_List(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            throw new NotImplementedException();
        }

        public List<InstitutionStatus> ExecuteQuery_Object(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<InstitutionStatus> GetAll(string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return institutionStatusDAO.GetAll(mfbCode);
        }

        public InstitutionStatus GetByCode(string mfbCode)
        {
            return institutionStatusDAO.GetByCode(mfbCode);
        }

        public InstitutionStatus GetByID(long ID, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return institutionStatusDAO.GetByID(ID, mfbCode);
        }

        public PagedList<InstitutionStatus> GetByPage(string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess, PaginationOrder paginationOrder = PaginationOrder.DESC, int page = 1, int pageSize = 10)
        {
            return institutionStatusDAO.GetByPage(mfbCode, databaseSource, paginationOrder, page, pageSize);
        }

        public InstitutionStatus InsertAndSave(InstitutionStatus _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return institutionStatusDAO.InsertAndSave(_object, mfbCode);
        }

        public InstitutionStatus Update(InstitutionStatus _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return institutionStatusDAO.Update(_object, mfbCode);
        }
    }
}
