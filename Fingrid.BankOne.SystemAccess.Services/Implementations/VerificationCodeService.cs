﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using Fingrid.BankOne.SystemAccess.Core.Models;
using Fingrid.BankOne.SystemAccess.DAO.Interfaces;
using Fingrid.BankOne.SystemAccess.Services.Interfaces;
using Fingrid.BankOne.SystemAccess.Utility;
using Fingrid.BankOne.SystemAccess.Utility.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Services.Implementations
{
    public class VerificationCodeService : IVerificationCodeService
    {
        private readonly IVerificationCodeDAO verificationCodeDAO;
        public VerificationCodeService(IVerificationCodeDAO verificationCodeDAO)
        {
            this.verificationCodeDAO = verificationCodeDAO;
        }
        public IEnumerable<VerificationCode> BulkInsertAndSave(List<VerificationCode> _objects, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return verificationCodeDAO.BulkInsertAndSave(_objects, mfbCode);
        }

        public void Delete(VerificationCode _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            verificationCodeDAO.Delete(_object, mfbCode);
        }

        public void ExecuteQuery(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            throw new NotImplementedException();
        }

        public List<VerificationCode> ExecuteQuery_List(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            throw new NotImplementedException();
        }

        public List<VerificationCode> ExecuteQuery_Object(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<VerificationCode> GetAll(string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return verificationCodeDAO.GetAll(mfbCode);
        }

        public VerificationCode GetByCode(string mfbCode, string code, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return verificationCodeDAO.GetByCode(mfbCode, code);
        }

        public VerificationCode GetByID(long ID, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return verificationCodeDAO.GetByID(ID, mfbCode);
        }

        public PagedList<VerificationCode> RetrievePaged(string mfbCode, string verificationCode, string generatedBy, string requestedBy, string status, int page, int perPage, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return verificationCodeDAO.RetrievePaged(mfbCode, verificationCode, generatedBy, requestedBy, status, page, perPage, databaseSource);
        }

        public PagedList<VerificationCode> GetByPage(string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess, PaginationOrder paginationOrder = PaginationOrder.DESC, int page = 1, int pageSize = 10)
        {
            return verificationCodeDAO.GetByPage(mfbCode, databaseSource, paginationOrder, page, pageSize);
        }

        public VerificationCode InsertAndSave(VerificationCode _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return verificationCodeDAO.InsertAndSave(_object, mfbCode);
        }

        public VerificationCode Update(VerificationCode _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return verificationCodeDAO.Update(_object, mfbCode);
        }

        public VerificationCode VerifyVerificationCode(string mfbCode, string code, out string response)
        {
            try
            {
                VerificationCode verificationCode = GetByCode(mfbCode, code);
                if (verificationCode != null)
                {
                    if (verificationCode.Status == SystemAccessCodeStatus.Generated)
                    {
                        verificationCode.Status = (((TimeSpan)(DateTime.Now - verificationCode.DateGenerated)).Days > 1 ? Core.Enums.SystemAccessCodeStatus.Expired : Core.Enums.SystemAccessCodeStatus.Verified);
                        verificationCode.DateVerified = DateTime.Now;
                        verificationCode = Update(verificationCode, mfbCode);
                        if (verificationCode.Status == SystemAccessCodeStatus.Expired)
                        {
                            response = VerificationCodeConstants.EXPIRED_VERIFICATION_CODE;
                            return null;
                        }
                        else
                        {
                            response = VerificationCodeConstants.VERIFICATIONCODE_VERIFIED_SUCCESSFULLY;
                            return verificationCode;
                        }

                        new Logger().Log(response);
                    }
                    else
                    {
                        response = VerificationCodeConstants.INVALID_VERIFICATION_CODE;
                        return null;
                    }

                    
                }
                else
                {
                    response = VerificationCodeConstants.VERIFICATIONCODE_NOT_FOUND;
                    return null;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                throw;
            }
           
        }
    }
}
