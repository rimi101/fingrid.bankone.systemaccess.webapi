﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using Fingrid.BankOne.SystemAccess.Core.Models;
using Fingrid.BankOne.SystemAccess.DAO.EF.Implementation;
using Fingrid.BankOne.SystemAccess.DAO.Interfaces;
using Fingrid.BankOne.SystemAccess.Services.Interfaces;
using Fingrid.BankOne.SystemAccess.Utility.Constants;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Services.Implementations
{
    public class ValidationCodeService : IValidationCodeService
    {
        private readonly IValidationCodeDAO validationCodeDAO;
        private readonly VerificationCodeService verificationCodeService;
        public ValidationCodeService(IValidationCodeDAO validationCodeDAO)
        {
            this.validationCodeDAO = validationCodeDAO;
            verificationCodeService = new VerificationCodeService(new VerificationCodeDAO());
        }
        public IEnumerable<ValidationCode> BulkInsertAndSave(List<ValidationCode> _objects, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return validationCodeDAO.BulkInsertAndSave(_objects, mfbCode);
        }

        public void Delete(ValidationCode _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            validationCodeDAO.Delete(_object, mfbCode);
        }

        public void ExecuteQuery(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            throw new NotImplementedException();
        }

        public List<ValidationCode> ExecuteQuery_List(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            throw new NotImplementedException();
        }

        public List<ValidationCode> ExecuteQuery_Object(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ValidationCode> GetAll(string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return validationCodeDAO.GetAll(mfbCode);
        }

        public ValidationCode GetByCode(string mfbCode, string code, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return validationCodeDAO.GetByCode(mfbCode, code, databaseSource);
        }

        public ValidationCode GetByID(long ID, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return validationCodeDAO.GetByID(ID, mfbCode);
        }

        public PagedList<ValidationCode> GetByPage(string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess, PaginationOrder paginationOrder = PaginationOrder.DESC, int page = 1, int pageSize = 10)
        {
            return validationCodeDAO.GetByPage(mfbCode, databaseSource, paginationOrder, page, pageSize);
        }

        public ValidationCode InsertAndSave(ValidationCode _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return validationCodeDAO.InsertAndSave(_object, mfbCode);
        }

        public ValidationCode Update(ValidationCode _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess)
        {
            return validationCodeDAO.Update(_object, mfbCode);
        }

        public bool ValidateValidationCode(string validationCode, string mfbCode, out string response)
        {
            string[] section = validationCode.Split('-');
            if (section[0] != mfbCode)
            {
                response = ValidationCodeConstants.VALIDATIONCODE_NOT_MATCH_INSTITUTIONCODE;
                return false;
            }
            else
            {
                DateTime _date = Convert.ToDateTime(section[2], CultureInfo.InvariantCulture);
                ValidationCode checkValCode = this.GetByCode(section[0], mfbCode);
                if (checkValCode == null)
                {
                    response = ValidationCodeConstants.INVALID_VALIDATION_CODE;
                    return false;
                }
                if (checkValCode.CreationDate != Convert.ToDateTime(section[2], CultureInfo.InvariantCulture))
                {
                    response = ValidationCodeConstants.VALIDATIONCODE_NOT_MATCH_CREATIONDATE;
                    return false;
                }
                else
                {
                    VerificationCode checkVerCode = verificationCodeService.GetByCode(mfbCode, checkValCode.VerificationCode);
                    if (checkVerCode == null)
                    {
                        response = ValidationCodeConstants.VALIDATIONCODE_VERIFICATIONCODE_NOT_FOUND;
                        return false;
                    }
                    else
                    {
                        if (checkVerCode.Status == SystemAccessCodeStatus.Enabled)
                        {
                            response = $"Access Granted for user with VerificationCode: {checkVerCode.Code}";
                            return true;
                        }
                        else
                        {
                            response = $"Access Denied for user with VerificationCode: {checkVerCode.Code}. {Environment.NewLine}UserStatus: {Enum.GetName(typeof(SystemAccessCodeStatus), checkVerCode.Status)}. {Environment.NewLine}Id = {checkVerCode.ID}";
                            return false;
                        }
                    }
                }
            }
        }
    }
}
