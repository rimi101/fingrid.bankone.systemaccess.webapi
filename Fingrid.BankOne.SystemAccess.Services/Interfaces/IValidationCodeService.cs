﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using Fingrid.BankOne.SystemAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Services.Interfaces
{
    public interface IValidationCodeService : IEntityService<ValidationCode>
    {
        ValidationCode GetByCode(string mfbCode, string code, DatabaseSource databaseSource = DatabaseSource.SystemAccess);

        bool ValidateValidationCode(string validationCode, string mfbCode, out string response);
    }
}
