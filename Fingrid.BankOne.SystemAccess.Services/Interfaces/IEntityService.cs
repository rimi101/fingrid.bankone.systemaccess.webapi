﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using Fingrid.BankOne.SystemAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Services.Interfaces
{
    public interface IEntityService<T> where T : Entity
    {
        IEnumerable<T> GetAll(string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess);

        PagedList<T> GetByPage(string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess, PaginationOrder paginationOrder = PaginationOrder.DESC, int page = 1, int pageSize = 10);

        T GetByID(long ID, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess);

        T InsertAndSave(T _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess);

        IEnumerable<T> BulkInsertAndSave(List<T> _objects, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess);

        void Delete(T _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess);

        T Update(T _object, string mfbCode, DatabaseSource databaseSource = DatabaseSource.SystemAccess);

        void ExecuteQuery(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess);

        List<T> ExecuteQuery_List(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess);

        List<T> ExecuteQuery_Object(string mfbCode, string query, DatabaseSource databaseSource = DatabaseSource.SystemAccess);

    }
}
