﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using Fingrid.BankOne.SystemAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Services.Interfaces
{
    public interface IVerificationCodeService : IEntityService<VerificationCode>
    {
        VerificationCode GetByCode(string mfbCode, string code, DatabaseSource databaseSource = DatabaseSource.SystemAccess);

        VerificationCode VerifyVerificationCode(string mfbCode, string code, out string response);
    }
}
