﻿using System;
using Fingrid.BankOne.SystemAccess.Core.Models;
using Fingrid.BankOne.SystemAccess.DAO.EF.Implementation;
using Fingrid.BankOne.SystemAccess.Services.Implementations;
using Fingrid.BankOne.SystemAccess.UnitTest.Fakers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Fingrid.BankOne.SystemAccess.UnitTest
{
    [TestClass]
    public class VerificationCodeTest
    {
        private readonly VerificationCodeService verificationCodeService;
        public VerificationCodeTest()
        {
            verificationCodeService = new VerificationCodeService(new VerificationCodeDAO());
        }

        [TestMethod]
        public VerificationCode CreateVerificationCodeTest()
        {
            VerificationCode verificationCode = VerificationCodeFaker.GenerateVerifcationCode();
            verificationCode.DateGenerated = DateTime.Now;

            var response = verificationCodeService.InsertAndSave(verificationCode, verificationCode.InstitutionCode);
            Assert.AreEqual(verificationCode, response);
            Console.WriteLine($"Saved VerificationCode: {JsonConvert.SerializeObject(verificationCode)}");
            return response;
        }

        [TestMethod]
        public void VerifyVerificationCodeTest()
        {

        }
    }
}
