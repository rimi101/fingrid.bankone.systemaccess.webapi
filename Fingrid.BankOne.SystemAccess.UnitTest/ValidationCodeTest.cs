﻿using System;
using Fingrid.BankOne.SystemAccess.Core.Models;
using Fingrid.BankOne.SystemAccess.DAO.EF.Implementation;
using Fingrid.BankOne.SystemAccess.Services.Implementations;
using Fingrid.BankOne.SystemAccess.UnitTest.Fakers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Fingrid.BankOne.SystemAccess.UnitTest
{
    [TestClass]
    public class ValidationCodeTest
    {
        private readonly ValidationCodeService validationCodeService;
        private readonly VerificationCodeService verificationCodeService;
        public ValidationCodeTest()
        {
            validationCodeService = new ValidationCodeService(new ValidationCodeDAO());
            verificationCodeService = new VerificationCodeService(new VerificationCodeDAO());
        }

        [TestMethod]
        public ValidationCode CreateValidationCodeTest()
        {
            ValidationCode validationCode = ValidationCodeFaker.GenerateValidationCode();
            validationCode.CreationDate = DateTime.Now;

            var response = validationCodeService.InsertAndSave(validationCode, validationCode.InstitutionCode);
            Assert.AreEqual(validationCode, response);
            Console.WriteLine($"Saved ValidationCode: {JsonConvert.SerializeObject(validationCode)}");
            return response;
        }

        [TestMethod]
        public void ValidateValidationCodeTest()
        {

        }

        [TestMethod]
        public void EnableValidationCode()
        {

        }
    }
}
