﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bogus;
using Fingrid.BankOne.SystemAccess.Core.Models;
using Fingrid.BankOne.SystemAccess.DAO.EF.Implementation;
using Newtonsoft.Json;
using static Bogus.DataSets.Name;

namespace Fingrid.BankOne.SystemAccess.UnitTest.Fakers
{
    public class VerificationCodeFaker
    {
        internal static VerificationCode GenerateVerifcationCode()
        {
            var verificationCode = new Faker<VerificationCode>()
                .RuleFor(u => u.Code, f => f.Random.Number(100000, 999999).ToString())
                .RuleFor(u => u.InstitutionCode, f => f.Random.Number(5, 5).ToString().PadLeft(4, '0'))
                //.RuleFor(u => u.DateGenerated, f => f.Date.Recent())
                .RuleFor(u => u.UserName, f => f.Name.FirstName((Gender)f.Random.Number(0, 1)).ToString().ToLower())
                .RuleFor(u => u.RequestBy, f => f.Name.FullName((Gender)f.Random.Number(0, 1)))
                .RuleFor(u => u.RequestPurpose, f => f.Lorem.Sentence(5))
                .FinishWith((f, u) =>
                {
                    Console.WriteLine($"Generated VerificationCode");
                });
            return verificationCode.Generate();
        }
    }
}
