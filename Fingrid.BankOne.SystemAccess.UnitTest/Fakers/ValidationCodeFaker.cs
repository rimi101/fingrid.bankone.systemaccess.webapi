﻿using Bogus;
using Fingrid.BankOne.SystemAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.UnitTest.Fakers
{
    public class ValidationCodeFaker
    {
        internal static ValidationCode GenerateValidationCode()
        {
            var validationCode = new Faker<ValidationCode>()
                .RuleFor(u => u.InstitutionCode, f => f.Random.Number(5, 5).ToString().PadLeft(4, '0'))
                .RuleFor(u => u.Code, f => f.Random.Number(100000, 999999).ToString())
                //.RuleFor(u => u.CreationDate, f => f.Date.Recent())
                .RuleFor(u => u.VerificationCode, f => f.Random.Number(100000, 999999).ToString())
            .FinishWith((f, u) =>
            {
                Console.WriteLine($"Generated ValidationCode");
            });
            return validationCode.Generate();
        }
    }
}
