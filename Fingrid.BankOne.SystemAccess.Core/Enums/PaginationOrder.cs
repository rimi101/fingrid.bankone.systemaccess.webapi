﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Core.Enums
{
    public enum PaginationOrder
    {
        DESC = 1,
        ASC = 2
    }
}
