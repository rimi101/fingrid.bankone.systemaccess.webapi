﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Core.Enums
{
    public enum SystemAccessCodeStatus
    {
        Generated = 1,
        Expired,
        Verified,
        Validated,
        Approved,
        NotValidated,
        Enabled,
        Disabled
    }
}
