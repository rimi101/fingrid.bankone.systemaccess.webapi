﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Core.Enums
{
    public enum DatabaseSource
    {
        Local = 0,
        Remote = 1,
        Core = 2,
        SystemAccess = 3
    }
}
