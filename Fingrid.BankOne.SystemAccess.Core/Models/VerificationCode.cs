﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Core.Models
{
    [Table("VerificationCodes")]
    public partial class VerificationCode : Entity
    {
        //[Required]
        //[StringLength(10)]
        //public virtual string InstitutionCode { get; set; }

        public virtual SystemAccessCodeStatus? Status { get; set; }

        [StringLength(50)]
        public virtual string UserName { get; set; }

        public virtual DateTime? DateGenerated { get; set; }

        public virtual DateTime? DateVerified { get; set; }
        
        [StringLength(50)]
        public virtual string Code { get; set; }

        [StringLength(50)]
        public virtual string RequestResponse { get; set; }

        [StringLength(50)]
        public virtual string RequestBy { get; set; }

        public virtual string RequestPurpose { get; set; }

    }
}
