﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Core.Models
{
    public class PagedList<T> where T : Entity
    {
        /// <summary>
        /// the list of generic data
        /// </summary>
        public IEnumerable<T> data { get; set; }
        /// <summary>
        /// the total number of items available in store
        /// </summary>
        public long total { get; set; }
        /// <summary>
        /// the current page being searched
        /// </summary>
        public int page { get; set; }
        /// <summary>
        /// maximum total number of items in result collection
        /// </summary>
        public int pagesize { get; set; }
    }
}
