﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Core.Models
{
    [Table("InstitutionStatuses")]
    public partial class InstitutionStatus : Entity
    {
        //[StringLength(50)]
        //public virtual string InstitutionCode { get; set; }

        public virtual string InstitutionName { get; set; }

        public virtual bool? Enabled { get; set; }
    }
}
