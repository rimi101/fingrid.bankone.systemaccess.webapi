﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Core.Models
{
    [Table("ValidationCodes")]
    public partial class ValidationCode : Entity
    {
        [StringLength(50)]
        public virtual string Code { get; set; }

        public virtual DateTime? CreationDate { get; set; }

        [StringLength(50)]
        public virtual string VerificationCode { get; set; }

        //[StringLength(10)]
        //public virtual string InstitutionCode { get; set; }
    }
}
