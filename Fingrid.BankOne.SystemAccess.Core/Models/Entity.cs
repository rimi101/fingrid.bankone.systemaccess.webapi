﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.Core.Models
{
    public class Entity
    {
        public virtual long ID { get; set; }

        [StringLength(10)]
        public virtual string InstitutionCode { get; set; }
    }
}
