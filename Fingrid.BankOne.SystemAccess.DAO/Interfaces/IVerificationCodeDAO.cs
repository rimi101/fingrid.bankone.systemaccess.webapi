﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using Fingrid.BankOne.SystemAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.DAO.Interfaces
{
    public interface IVerificationCodeDAO : IEntityDAO<VerificationCode>
    {
        VerificationCode GetByCode(string mfbCode, string code, DatabaseSource databaseSource = DatabaseSource.SystemAccess);
        PagedList<VerificationCode> RetrievePaged(string mfbCode, string verificationCode, string generatedBy, string requestedBy, string status, int page, int perPage, DatabaseSource databaseSource);
    }
}
