﻿using Fingrid.BankOne.SystemAccess.Core.Enums;
using Fingrid.BankOne.SystemAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.DAO.Interfaces
{
    public interface IValidationCodeDAO : IEntityDAO<ValidationCode>
    {
        ValidationCode GetByCode(string mfbCode, string code, DatabaseSource databaseSource = DatabaseSource.SystemAccess);
    }
}
