﻿using Fingrid.BankOne.SystemAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingrid.BankOne.SystemAccess.DAO.Interfaces
{
    public interface IInstitutionStatusDAO : IEntityDAO<InstitutionStatus>
    {
        InstitutionStatus GetByCode(string mfbCode);
    }
}
